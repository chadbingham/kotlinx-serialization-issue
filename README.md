This _should_ be ready to run. You may need to install npm modules: 

`cd  js-module/functions && npm install && cd -`

To see the error, run this command:

`./gradlew :js-module:build`

If that doesnt show and error, run this:

`./gradlew :js-module:test`

_______

**Output:** 

```
1) sample
       SerializationTest
         writeObject:
     TypeError: this.descriptor.addElement_ivxn3r$ is not a function
      at new FooBar$$serializer (functions/src/test/index_test.js:38:21)
      at FooBar$$serializer_getInstance (functions/src/test/index_test.js:98:7)
      at FooBar$Companion.serializer (functions/src/test/index_test.js:22:12)
      at SerializationTest.writeObject (functions/src/test/index_test.js:128:97)
      at Context.<anonymous> (functions/src/test/index_test.js:149:42)

```

[This was reported](https://github.com/Kotlin/kotlinx.serialization/issues/261)