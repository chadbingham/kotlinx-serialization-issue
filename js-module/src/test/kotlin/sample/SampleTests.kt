package sample

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test

@Serializable
class FooBar(val s: String, val i: Int, val b: Boolean)

class SerializationTest {
    @Test
    fun writeObject() {
        val fooBar = FooBar("we didnt start the fire", 1989, true)
        console.dir(fooBar)
        val serialized = JSON.indented.stringify(FooBar.serializer(), fooBar)
        console.error("serialized: $serialized")

        /* writing to firebase removed for brevity */

    }
}